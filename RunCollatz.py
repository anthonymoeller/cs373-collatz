#!/usr/bin/env python3

# -------------
# RunCollatz.py
# -------------
"""
Executable for Collatz algorithm, run to begin taking command line
or file based arguments
"""

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
